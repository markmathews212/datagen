from flask import Flask, Response
from flask_cors import CORS
import requests

from RandomDealData import RandomDealData

app = Flask(__name__)
CORS(app)
random_deal_data = RandomDealData()
instrument_list = random_deal_data.createInstrumentList()

DAO_URL = 'dao-v1.blackjackroot.svc.cluster.local:8080'


@app.route('/')
def index():
    def eventStream():
        while True:
            data = random_deal_data.createRandomData(instrument_list)
            # requests.post(url=DAO_URL, data=data)
            yield "\"data\":{}\n\n".format(data)  # For SSE, have to return a string object
    return Response(eventStream(), status=200, mimetype='text/event-stream')


def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))


if __name__ == '__main__':
    bootapp()
