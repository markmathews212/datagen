import pytest
from .. import RandomDealData as RandomDealData

instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
               "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")

random_deal_data = RandomDealData.RandomDealData()
instrument_list = random_deal_data.createInstrumentList()
inst = instrument_list[0]
# random_data = random_deal_data.createRandomData(instrument_list)
# data_json = json.loads(random_data)


def test_instrument_not_null_name():
    assert isinstance(inst.name, str)


def test_instrument_not_null_variance():
    assert isinstance(inst.getVariance(), float)


def test_instrument_not_null_drift():
    assert isinstance(inst.getDrift(), float)


def test_instrument_not_null_price():
    assert isinstance(inst.getPrice(), float)


def test_instrument_not_null_starting_price():
    assert isinstance(inst.getStartingPrice(), float)

def test_instrument_calculateNextPrice1():
    assert isinstance(inst.calculateNextPrice('B'), float)

def test_instrument_calculateNextPrice2():
    assert isinstance(inst.calculateNextPrice('S'), float)





